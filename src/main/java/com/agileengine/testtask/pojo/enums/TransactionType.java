package com.agileengine.testtask.pojo.enums;

public enum TransactionType {
    DEBIT, CREDIT
}
