package com.agileengine.testtask.pojo;

import com.agileengine.testtask.model.TransactionTO;
import com.agileengine.testtask.model.UserTO;
import com.agileengine.testtask.pojo.enums.TransactionType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TransactionHistoryResponseTO {
    private UserTO user;
    private List<TransactionTO> transactions;
}
