package com.agileengine.testtask.pojo;

import com.agileengine.testtask.model.TransactionTO;
import com.agileengine.testtask.model.UserTO;
import lombok.Data;

import java.util.List;

import static java.util.Objects.isNull;

@Data
public class Singleton {

    private UserTO user;
    private List<TransactionTO> transactions;

    //EAGER INITIALIZATION
    /*
    private static final Singleton INSTANCE = new Singleton(); //This is safe thread

    public static Singleton getInstance() {
        return INSTANCE;
    }*/

    //Lazy initialization with Double check locking
    private static Singleton INSTANCE;

    public static Singleton getInstance() {
        if (isNull(INSTANCE)) {
            //synchronized block to remove overhead
            synchronized (Singleton.class) {
                if (isNull(INSTANCE)) {
                    // if instance is null, initialize
                    INSTANCE = new Singleton();
                }

            }
        }
        return INSTANCE;
    }

}
