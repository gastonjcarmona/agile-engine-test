package com.agileengine.testtask.pojo.exception;

import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
public class BusinessStateException extends RuntimeException {

    private HttpStatus status;

    public BusinessStateException(final String message, final HttpStatus status){
        super(message);
        this.status = status;
    }

}
