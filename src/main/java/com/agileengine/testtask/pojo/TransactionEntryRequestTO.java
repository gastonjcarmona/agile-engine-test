package com.agileengine.testtask.pojo;

import com.agileengine.testtask.pojo.enums.Currency;
import com.agileengine.testtask.pojo.enums.TransactionType;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TransactionEntryRequestTO {

    @NotNull
    @ApiParam(required = true)
    @Positive
    private BigDecimal amount;

    @NotNull
    @ApiParam(required = true)
    private TransactionType type;

    @NotNull
    @ApiParam(required = true)
    private Currency currency;
}
