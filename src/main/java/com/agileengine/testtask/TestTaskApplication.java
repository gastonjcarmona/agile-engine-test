package com.agileengine.testtask;

import com.agileengine.testtask.service.MainService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import javax.annotation.PostConstruct;
import java.util.TimeZone;

import static java.time.ZoneOffset.UTC;

@Slf4j
@EnableSwagger2
@SpringBootApplication
//@EnableTransactionManagement
public class TestTaskApplication {

    @Autowired
    private MainService mainService;

    public static void main(final String[] args) {
        SpringApplication.run(TestTaskApplication.class, args);
    }

    @PostConstruct
    public void init() {
        log.info("Setting UTC timezone");
        log.info("Old timezone {}", TimeZone.getDefault());
        TimeZone.setDefault(TimeZone.getTimeZone(UTC));
        log.info("New timezone {}", TimeZone.getDefault());

        mainService.main();

    }
}
