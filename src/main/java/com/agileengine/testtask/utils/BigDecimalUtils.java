package com.agileengine.testtask.utils;

import java.math.BigDecimal;

public class BigDecimalUtils {

    public static boolean isPositive(final BigDecimal number) {
        return number.compareTo(BigDecimal.ZERO) > 0;
    }
}
