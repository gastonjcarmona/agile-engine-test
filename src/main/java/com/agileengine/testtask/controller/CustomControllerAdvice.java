package com.agileengine.testtask.controller;

import com.agileengine.testtask.pojo.exception.BusinessStateException;
import com.agileengine.testtask.pojo.ErrorResponseTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;


import static java.util.stream.Collectors.joining;
import static org.springframework.http.HttpStatus.*;
import static org.springframework.http.ResponseEntity.badRequest;
import static org.springframework.http.ResponseEntity.status;

@Slf4j
@ControllerAdvice
public class CustomControllerAdvice extends ResponseEntityExceptionHandler {

    private static final String VALIDATION_EXCEPTION_MESSAGE = "Validation exception: '{}'";

    /**
     * Handle entity exceptions.
     */
    @ExceptionHandler({BusinessStateException.class})
    ResponseEntity<ErrorResponseTO> businessStateException(final BusinessStateException e) {
        log.warn("Business State Exception", e);
        return status(e.getStatus()).body(ErrorResponseTO.builder().status(e.getStatus()).message(e.getMessage()).build());
    }


    /**
     * Handle unknown exceptions.
     */
    @ExceptionHandler({Exception.class})
    public ResponseEntity<ErrorResponseTO> unknownException(final Exception e) {
        log.error("Unknown exception", e);
        return status(INTERNAL_SERVER_ERROR).body(ErrorResponseTO.builder().status(INTERNAL_SERVER_ERROR).message(e.getMessage()).build());
    }

    /**
     * Handle validation annotations on DTOs.
     */
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(final MethodArgumentNotValidException ex,
                                                                  final HttpHeaders headers, final HttpStatus status,
                                                                  final WebRequest request) {
        final String message = ex.getBindingResult().getAllErrors().stream().map(DefaultMessageSourceResolvable::getDefaultMessage).collect(joining(", "));
        log.warn(VALIDATION_EXCEPTION_MESSAGE, message);
        return badRequest().body(ErrorResponseTO.builder().message(message).status(BAD_REQUEST).build());
    }


    /**
     * Handle illegal field values in incoming json request.
     */
    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(final HttpMessageNotReadableException e,
                                                                  final HttpHeaders headers, final HttpStatus status,
                                                                  final WebRequest request) {
        log.warn(VALIDATION_EXCEPTION_MESSAGE, e.getMessage());
        return badRequest().body(ErrorResponseTO.builder().message(e.getMessage()).status(BAD_REQUEST).build());
    }

}
