package com.agileengine.testtask.controller;

import com.agileengine.testtask.model.TransactionTO;
import com.agileengine.testtask.pojo.TransactionEntryRequestTO;
import com.agileengine.testtask.pojo.TransactionHistoryResponseTO;
import com.agileengine.testtask.service.TransactionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Slf4j
@RestController
@RequestMapping(path = "/api/v1/transaction", produces = MediaType.APPLICATION_JSON_VALUE)
@Api(tags = "Transactions")
public class TransactionController {

    @Autowired
    private TransactionService transactionService;

    @ApiOperation("Get Transaction History")
    @GetMapping("/history")
    public TransactionHistoryResponseTO getTransactionHistory() {
        log.info("Get Transaction History");
        return transactionService.getTransactionHistory();
    }

    @ApiOperation("Get Transaction by id")
    @GetMapping("/{id}")
    public TransactionTO getTransactionById(@PathVariable final String id) {
        log.info("Get Transaction History for account number: '{}'", id);
        return transactionService.getTransactionById(id);
    }

    @ApiOperation("Create New Transaction")
    @PostMapping
    public TransactionTO createTransactionEntry(@RequestBody @Valid final TransactionEntryRequestTO request) {
        log.info("Create Transaction Entry. Request: '{}'", request);
        return transactionService.createTransaction(request);
    }
}
