package com.agileengine.testtask.model;

import com.agileengine.testtask.pojo.enums.Currency;
import com.agileengine.testtask.pojo.enums.TransactionType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
public class TransactionTO extends BaseEntityTO {
    private String id;
    private String userId;
    private TransactionType type;
    private Currency currency;
    private BigDecimal amount;
    private LocalDateTime effectiveDate;
}
