package com.agileengine.testtask.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
public class UserTO extends BaseEntityTO {
    private String id;
    private String name;
    private String lastName;
    private BigDecimal balance;
}
