package com.agileengine.testtask.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

@Data
@EqualsAndHashCode
public class BaseEntityTO {
    private LocalDateTime created;
    private LocalDateTime updated;
}
