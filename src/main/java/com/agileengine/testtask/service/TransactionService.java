package com.agileengine.testtask.service;

import com.agileengine.testtask.model.TransactionTO;
import com.agileengine.testtask.pojo.exception.BusinessStateException;
import com.agileengine.testtask.pojo.Singleton;
import com.agileengine.testtask.pojo.TransactionEntryRequestTO;
import com.agileengine.testtask.pojo.TransactionHistoryResponseTO;
import com.agileengine.testtask.pojo.enums.TransactionType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

import static com.agileengine.testtask.pojo.enums.TransactionType.CREDIT;
import static com.agileengine.testtask.pojo.enums.TransactionType.DEBIT;
import static com.agileengine.testtask.utils.BigDecimalUtils.isPositive;
import static org.apache.commons.collections4.CollectionUtils.emptyIfNull;

@Slf4j
@Service
public class TransactionService {

    public TransactionTO createTransaction(final TransactionEntryRequestTO request) {
        validateAmount(request.getAmount());

        final TransactionTO transaction = new TransactionTO();
        transaction.setId(UUID.randomUUID().toString());
        transaction.setCreated(LocalDateTime.now());
        transaction.setEffectiveDate(LocalDateTime.now());
        transaction.setAmount(request.getAmount());
        transaction.setType(request.getType());
        transaction.setCurrency(request.getCurrency());

        final Singleton singleton = Singleton.getInstance();
        transaction.setUserId(singleton.getUser().getId());

        //singleton.getUser().setBalance(calculateNewBalance(singleton.getUser().getBalance(), request.getAmount(), request.getType()));
        singleton.getUser().setBalance(calculateNewBalance(request.getAmount(), request.getType()));
        singleton.getUser().setUpdated(LocalDateTime.now());
        singleton.getTransactions().add(transaction);

        log.info("New transaction created. '{}'", transaction);
        return transaction;

    }

    public TransactionHistoryResponseTO getTransactionHistory() {
        final Singleton singleton = Singleton.getInstance();
        return TransactionHistoryResponseTO.builder()
                .user(singleton.getUser())
                .transactions(singleton.getTransactions())
                .build();
    }

    public TransactionTO getTransactionById(final String id) {
        return Singleton.getInstance().getTransactions().stream().filter(t -> t.getId().equals(id)).findFirst().orElseThrow(() -> new BusinessStateException("Id not found", HttpStatus.NOT_FOUND));
    }


    public BigDecimal calculateNewBalance(final BigDecimal balance, final BigDecimal amount, final TransactionType type) {
        BigDecimal newBalance;

        if (DEBIT.equals(type)) {
            newBalance = balance.subtract(amount);
        } else {
            newBalance = balance.add(amount);
        }

        validateBalance(newBalance);
        return newBalance;
    }

    public BigDecimal calculateNewBalance(final BigDecimal amount, final TransactionType type) {
        final Singleton singleton = Singleton.getInstance();

        final BigDecimal credit = emptyIfNull(singleton.getTransactions())
                .stream()
                .filter(t -> CREDIT.equals(t.getType()))
                .map(TransactionTO::getAmount)
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        final BigDecimal debit = emptyIfNull(singleton.getTransactions())
                .stream()
                .filter(t -> DEBIT.equals(t.getType()))
                .map(TransactionTO::getAmount)
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        final BigDecimal actualBalance = credit.subtract(debit);
        BigDecimal newBalance;

        if (DEBIT.equals(type)) {
            newBalance = actualBalance.subtract(amount);
        } else {
            newBalance = actualBalance.add(amount);
        }

        validateBalance(newBalance);
        return newBalance;
    }

    private void validateAmount(final BigDecimal amount) {
        if (!isPositive(amount)) {
            throw new BusinessStateException("Amount must be positive", HttpStatus.BAD_REQUEST);
        }
    }

    private void validateBalance(final BigDecimal balance) {
        if (!isPositive(balance)) {
            throw new BusinessStateException("Invalid Amount. You do not have sufficient balance", HttpStatus.CONFLICT);
        }
    }
}
