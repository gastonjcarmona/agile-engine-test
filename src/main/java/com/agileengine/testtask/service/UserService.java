package com.agileengine.testtask.service;

import com.agileengine.testtask.model.UserTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

@Slf4j
@Service
public class UserService {

    public UserTO createUser(final String name, final String lastName) {
        final UserTO user = new UserTO();
        user.setId(UUID.randomUUID().toString());
        user.setName(name);
        user.setLastName(lastName);
        user.setCreated(LocalDateTime.now());
        user.setBalance(BigDecimal.ZERO);

        log.info("New user created. '{}'", user);
        return user;
    }
}
