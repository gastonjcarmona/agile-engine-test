package com.agileengine.testtask.service;

import com.agileengine.testtask.model.UserTO;
import com.agileengine.testtask.pojo.Singleton;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static com.google.common.collect.Lists.newArrayList;

@Slf4j
@Service
public class MainService {

    @Autowired
    private UserService userService;

    public void main() {
        final UserTO user = userService.createUser("Gaston", "Carmona");
        log.info("Initial User '{}'", user);

        Singleton.getInstance().setUser(user);
        Singleton.getInstance().setTransactions(newArrayList());
    }
}
