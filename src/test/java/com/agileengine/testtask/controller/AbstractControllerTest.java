package com.agileengine.testtask.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.testng.annotations.BeforeClass;

import static com.fasterxml.jackson.databind.DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES;
import static com.fasterxml.jackson.databind.MapperFeature.DEFAULT_VIEW_INCLUSION;
import static com.fasterxml.jackson.databind.SerializationFeature.WRITE_DATES_AS_TIMESTAMPS;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

public abstract class AbstractControllerTest {

    private MockMvc mockMvc;
    private ObjectMapper objectMapper;

    @BeforeClass
    public void init() {
        initMocks(this);
        this.mockMvc = standaloneSetup(this.getTarget()).build();
        this.objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        objectMapper.disable(FAIL_ON_UNKNOWN_PROPERTIES);
        objectMapper.disable(WRITE_DATES_AS_TIMESTAMPS);
        objectMapper.enable(DEFAULT_VIEW_INCLUSION);
    }

    protected abstract Object getTarget();

    protected <I> void perform(final MockHttpServletRequestBuilder requestBuilder, final I request, final ResultMatcher status) throws Exception {
        performRequest(requestBuilder, request, status);
    }

    protected <I, O> O perform(final MockHttpServletRequestBuilder requestBuilder, final I request, final Class<O> aclass, final ResultMatcher status) throws Exception {
        final MvcResult result = performRequest(requestBuilder, request, status);
        return (aclass != null) ? objectMapper.readValue(result.getResponse().getContentAsString(), aclass) : null;
    }

    protected <I, O> O perform(final MockHttpServletRequestBuilder requestBuilder, final I request, final TypeReference<O> typeReference, final ResultMatcher status) throws Exception {
        final MvcResult result = performRequest(requestBuilder, request, status);
        return (typeReference != null) ? objectMapper.readValue(result.getResponse().getContentAsString(), typeReference) : null;
    }

    private <I> MvcResult performRequest(final MockHttpServletRequestBuilder requestBuilder, final I request, final ResultMatcher status) throws Exception {
        final String json = objectMapper.writeValueAsString(request);

        return this.mockMvc.perform(
                requestBuilder
                        .content(json)
                        .contentType(APPLICATION_JSON))
                .andExpect(status)
                .andReturn();
    }

}