package com.agileengine.testtask.controller;

import com.agileengine.testtask.model.TransactionTO;
import com.agileengine.testtask.pojo.TransactionEntryRequestTO;
import com.agileengine.testtask.service.TransactionService;
import org.jeasy.random.EasyRandom;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.testng.annotations.Test;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Test
public class TransactionControllerTest extends AbstractControllerTest {

    private static final EasyRandom EASY_RANDOM = new EasyRandom();

    @Spy
    @InjectMocks
    private TransactionController target;

    @Mock
    private TransactionService transactionService;

    @Override
    protected Object getTarget() {
        return target;
    }

    public void getTransactionHistory() throws Exception {
        perform(get("/api/v1/transaction/history"), null, status().isOk());
    }

    public void getTransactionById() throws Exception {
        perform(get("/api/v1/transaction/1"), null, status().isOk());
    }

    public void createTransactionEntry() throws Exception {
        final TransactionEntryRequestTO request = EASY_RANDOM.nextObject(TransactionEntryRequestTO.class);
        when(transactionService.createTransaction(eq(request))).thenReturn(EASY_RANDOM.nextObject(TransactionTO.class));
        perform(post("/api/v1/transaction"), request, status().isOk());
    }

}
