package com.agileengine.testtask.service;

import com.agileengine.testtask.model.TransactionTO;
import com.agileengine.testtask.model.UserTO;
import com.agileengine.testtask.pojo.exception.BusinessStateException;
import com.agileengine.testtask.pojo.Singleton;
import com.agileengine.testtask.pojo.TransactionEntryRequestTO;
import com.agileengine.testtask.pojo.TransactionHistoryResponseTO;
import com.agileengine.testtask.pojo.enums.Currency;
import com.agileengine.testtask.pojo.enums.TransactionType;
import org.jeasy.random.EasyRandom;
import org.mockito.InjectMocks;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import static com.google.common.collect.Lists.newArrayList;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

@Test
public class TransactionServiceTest {

    private static final EasyRandom EASY_RANDOM = new EasyRandom();

    @InjectMocks
    private TransactionService target;

    @BeforeClass
    public void beforeClass() {
        initMocks(this);
    }

    @BeforeMethod
    public void beforeMethod() {
        final UserTO user = new UserTO("1", "Gaston", "Carmona", BigDecimal.ZERO);
        Singleton.getInstance().setUser(user);
        
        Singleton.getInstance().setTransactions(newArrayList());

    }

    public void createTransaction() {
        final TransactionEntryRequestTO request = EASY_RANDOM.nextObject(TransactionEntryRequestTO.class);
        request.setType(TransactionType.CREDIT);
        request.setAmount(BigDecimal.TEN);

        final TransactionTO response = target.createTransaction(request);
        assertNotNull(response);
        assertEquals(response.getType(), request.getType());
        assertEquals(response.getAmount(), request.getAmount());
        assertEquals(response.getCurrency(), request.getCurrency());
        assertEquals(Singleton.getInstance().getUser().getBalance(), request.getAmount());
    }

    public void getTransactionById() {
        final UserTO user = Singleton.getInstance().getUser();
        final TransactionTO transaction = new TransactionTO("1", user.getId(), TransactionType.CREDIT, Currency.USD, BigDecimal.valueOf(1), LocalDateTime.now());
        Singleton.getInstance().getTransactions().add(transaction);
        final TransactionTO response = target.getTransactionById("1");
        assertNotNull(response);
    }

    public void getTransactionHistory() {
        final TransactionHistoryResponseTO response = target.getTransactionHistory();
        assertNotNull(response);
    }

    public void calculateNewBalance() {
        final BigDecimal result = target.calculateNewBalance(BigDecimal.ZERO, BigDecimal.TEN, TransactionType.CREDIT);
        assertEquals(result, BigDecimal.TEN);
    }

    @Test(expectedExceptions = BusinessStateException.class)
    public void calculateNewBalanceNegativeError() {
        target.calculateNewBalance(BigDecimal.ZERO, BigDecimal.TEN, TransactionType.DEBIT);
    }


    public void calculateNewBalanceAlt() {
        final BigDecimal result = target.calculateNewBalance( BigDecimal.TEN, TransactionType.CREDIT);
        assertEquals(result, BigDecimal.TEN);
    }

    @Test(expectedExceptions = BusinessStateException.class)
    public void calculateNewBalanceNegativeErrorAlt() {
        target.calculateNewBalance(BigDecimal.TEN, TransactionType.DEBIT);
    }


}
