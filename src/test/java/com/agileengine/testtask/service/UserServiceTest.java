package com.agileengine.testtask.service;

import com.agileengine.testtask.model.UserTO;
import org.jeasy.random.EasyRandom;
import org.mockito.InjectMocks;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

@Test
public class UserServiceTest {

    @InjectMocks
    private UserService target;

    @BeforeClass
    public void beforeClass() {
        initMocks(this);
    }

    public void createUser() {
        final String name = "Gaston";
        final String lastName = "Carmona";

        final UserTO response = target.createUser(name, lastName);
        assertNotNull(response);
        assertEquals(response.getName(), name);
        assertEquals(response.getLastName(), lastName);
    }
}
