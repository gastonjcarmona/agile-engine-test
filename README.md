# Accounting Notebook

## Project Information

### Technologies
Solution was developed on *JAVA 1.8*, *Sprint Boot 2.1.4*.
For unit test I used *Testng* y *JUnit*.
Swagger Documentation was generated.
Authentication used *Basic Auth*. User: developer. Password: developer
Repository *Bitbucket*.

## ENDPOINTS
1. http://localhost:8080/swagger-ui.html
2. GET http://localhost:8080/api/v1/transactions/history
3. GET http://localhost:8080/api/v1/transactions/{id}
4. POST http://localhost:8080/api/v1/transactions


### Steps to running project
1. Clone repository from master.
2. run mvn install.
3. run project.

On console log you must view a basic running. I had to do this basic running because I had no more time to complete the exercise. I tried to follow the rules, so I used 3 hours. I miss the part of saving in memory the information with the concurrence.

I honestly did not know how to save the information in memory and make it transactional. I am used to using a transactional database like MySql for example. I will investigate how to implement singletons for these cases
